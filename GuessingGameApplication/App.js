import { StatusBar } from 'expo-status-bar';
import { ImageBackground, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import StartScreen from './Screen/StartScreen';
import { LinearGradient } from 'expo-linear-gradient';
import { useState} from 'react';
import GameScreen from './Screen/GameScreen'
import Colors from './constant/Colors';
import GameOverScreen from './Screen/GameOverScreen';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';

export default function App() {
  const [userNumber, setUserNumber] =useState()
  const [gameIsOver, setGameIsOver] =useState(true);//innitialize the value to true
  const [guessRounds, setGuessRounds] = useState(0)

  const[fontsLoaded] =useFonts({
    'open-sans' :require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  })



  if(!fontsLoaded){
    return <AppLoading/>
  }
  function startNewGameHandeler(){
    setUserNumber(null),
    setGuessRounds(0);
  }


  function pickerNumberHandeler(pickerNumber){
    setUserNumber(pickerNumber);
    setGameIsOver(false);
  }
  function gameOverHandler(numberOfRound){
    setGameIsOver(true)
    setGuessRounds(numberOfRound)
  }
  let screen =<StartScreen onPickNumber={pickerNumberHandeler}/>

  if(userNumber){
      screen =<GameScreen  userNumber={userNumber} onGameOver = {gameOverHandler}/>
  }
  if(gameIsOver && userNumber){
    screen =<GameOverScreen
              userNumber={userNumber}
              roundsNumber={guessRounds}
              onStartNewgame={startNewGameHandeler}/>
  }
    return (
      <>
        <StatusBar />
        <LinearGradient style={styles.container} colors={[Colors.primary500,Colors.accent500]}>
            <ImageBackground
            source ={require('./assets/successImage.jpg')}
            resizeMode= 'cover'
            style={styles.rootScreen}
            imageStyle= {styles.backgroundImage}

            >
          <SafeAreaView>
          {screen}
          </SafeAreaView>
          </ImageBackground>
          
        </LinearGradient>
       </>
  );
}

const styles = StyleSheet.create({
  container: {
  

   flex:1,
  },
});
