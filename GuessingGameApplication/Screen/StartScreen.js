import React, { useState }   from 'react';
import { Alert, Dimensions, KeyboardAvoidingView, ScrollView, StyleSheet, Text, TextInput, useWindowDimensions, View } from 'react-native';

import PrimaryButton from '../components/ui/PrimaryButton';
import Title from '../components/ui/Title';
import Colors from '../constant/Colors';
import Card from '../components/ui/Card';
import InstructionText from '../components/ui/InstructionText';


function StartScreen({onPickNumber}) {
  const [enteredNumber, setEnterNumber] =useState('');
  const {width, height} =useWindowDimensions();

  function numberInputHandeler (enterText){
    setEnterNumber (enterText);
  }
  function resetInputHandeler(){
    setEnterNumber('')
  }
  function confirmInputHandeler(){
    const chosenNumber =parseInt(enteredNumber)

    if(isNaN (chosenNumber) || chosenNumber <= 0 || chosenNumber > 99){
      Alert.alert('Invalid Number', 'Number has to be between 1 to 99',
      [{text: 'okay', style: 'destructive', onPress: resetInputHandeler}]
      )
    }
    // console.log(chosenNumber)
    onPickNumber(chosenNumber)

  }
  const marginTopDistance = height <380 ? 30 : 100;
  return (
    <ScrollView style ={styles.screen}>
    <KeyboardAvoidingView style={styles.screen} /* behavior='position'*/>
    <View style={[styles.rootContainer, {marginTop: marginTopDistance}]}>
      <Title>Guess My Number</Title>

      <Card style={styles.inputContainer}>
        <InstructionText>Enter a Number</InstructionText>
      <TextInput
      style={styles.numberInput}
      keyboardType='number-pad'
      maxLength={2} 
      autoCapitalize='none'
      autoCorrect={false}
      value={enteredNumber}
      onChangeText={numberInputHandeler}
      />
     <View style={styles.buttonsContainer}>
       <View style={styles.buttonContainer}>
          <PrimaryButton onPress={resetInputHandeler}>Reset</PrimaryButton>
       </View>
       <View style={styles.buttonContainer}>
       <PrimaryButton onPress={confirmInputHandeler}>Confirm</PrimaryButton>
       </View>
     </View>
      </Card>
    </View>
    </KeyboardAvoidingView>
    </ScrollView>
  );
}
export default StartScreen
const deviceWidth =Dimensions.get('window').width;

const styles = StyleSheet.create({
  rootContainer:{
    alignItems:'center',
    marginTop:deviceWidth < 380 ? 30: 100,
    flex: 1,
  },
  screen:{
    flex: 1,

  },
  instructionText:{
    color: Colors.accent500,
    fontSize: 24,
    
  },

  inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginTop:36,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor:Colors.primary500,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25
  },
  numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:Colors.accent500,
    borderBottomWidth: 2,
    color: Colors.accent500,
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
},
buttonsContainer: {
    flexDirection: 'row'
},
buttonContainer: {
    flex: 1,
}
});

