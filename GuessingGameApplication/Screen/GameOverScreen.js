import React from 'react'
import {Text, View, StyleSheet, Image, Dimensions, useWindowDimensions, ScrollView} from 'react-native'
import PrimaryButton from '../components/ui/PrimaryButton';
import Title from '../components/ui/Title';
import Colors from '../constant/Colors';
export default function GameOverScreen({roundsNumber,userNumber, onStartNewgame}) {
  const {width, height}=useWindowDimensions();
  let imageSize = 300;

  if (width < 380){
    imageSize =150;
  }
  if (height < 400){
    imageSize =80;
  }
  const imageStyle ={
    widthL: imageSize,
    height: imageSize,
    borderRadius: imageSize /2
  }
  return (
    <ScrollView style={styles.screen}>
    <View style={styles.rootContainer}>
      
        <Title>Game is over</Title>
        <View style={styles.imageContainer}>
        <Image source={require('../assets/over.gif')} style={styles.image}/>
        </View>
          <Text style={styles.summaryText}>
            Your phone needs <Text style={styles.heightLight}>{roundsNumber}</Text> number to guess the number <Text style={styles.heightLight}>{userNumber}</Text>
          </Text>
          <PrimaryButton onPress={onStartNewgame}>Start the New Game</PrimaryButton>
    </View>
    </ScrollView>
  )
}
const deviceWidth =Dimensions.get('window').width;


const styles= StyleSheet.create({
  rootContainer: {
  flex: 1,
  padding: 24,
  alignItems: 'center',
  justifyContent: 'center'
  },
  screen:{
    flex: 1,  
  },
  heightLight:{
    fontFamily:'open-sans-bold',
    color: Colors.primary500,


  },
  imageContainer: {
  width: deviceWidth < 380 ? 150: 300,
  height: deviceWidth < 380 ? 150: 300,
  borderRadius: deviceWidth < 380 ? 75: 150,
  borderWidth: 3,
  borderColor: Colors.primary800,
  overflow: 'hidden',
  margin: 36,
  },
  image: {
  height:"100%",
  width: '100%',
  },
  summaryText:{
    // fontFamily:"", 
    fontSize:24,
    textAlign: 'center',
    marginBottom:24,
  }
})
