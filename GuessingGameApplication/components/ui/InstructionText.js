import React from 'react'
import Colors from '../../constant/Colors'
import {StyleSheet, Text} from 'react-native';

export default function InstructionText({children, style}) {
  return (
    <Text style={[StyleSheet.instructionText, style]}>{children}</Text>
  )
}

const styles = StyleSheet.create({
    instructionText: {
        color: Colors.accent500,
        fontSize: 20,
        fontFamily:'open-sans-bold',
    }
})
