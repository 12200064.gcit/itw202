import React from 'react'
// import { Colors } from 'react-native/Libraries/NewAppScreen'
import Colors from '../../constant/Colors'
import {Platform, StyleSheet, Text, View} from 'react-native'

const Title =({children})=> {
  return (
    <Text style={styles.title}>{children}</Text>
  )
}
export default Title
const styles = StyleSheet.create({
    title:{
      fontFamily:'open-sans-bold',
        padding: 12,
        // borderWidth: Platform.OS === 'android' ? 2 : 0,
        // borderWidth: Platform.select({ios: 0, android: 2}),
        borderColor:'white',
        textAlign: 'center',
        color: 'white',
        fontSize: 24,
        // fontWeight: 'bold',
        maxWidth: '80%',
        width: 300,
    }

})
