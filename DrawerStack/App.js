import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import React, { Component } from "react";
import {Link} from "native-base";


import {
  Content,
  Text,
  Header,
  Container, 
  Title, 
   Left, 
   Right, 
   Body, 
   Icon,

} from 'native-base';


const Drawer =createDrawerNavigator();

function HomeScreen({navigation}) {
  
  return (
    <Container>
      <Header>
        <Left style={{flex: 0.1}}></Left>
        <Body style={{flex: 1, alignItems:'center'}}>
          <Title>Home</Title>
        </Body>
        <Right style={{flex: 0.1}}/>
      </Header>
      <Content 
      contentContainerStyle={{
        flex: 1, 
        alignItems:'center', 
        justifyContent:'center'}}>
          <Text>Home Screen</Text>

      </Content>
    </Container>
  );
}


function ProfileScreen({navigation}){
  return (
    <Container>
      <Header>
        <Left style={{flex: 0.1}}></Left>
        <Body style={{flex: 1, alignItems:'center'}}>
          <Title>Profile</Title>
        </Body>
        <Right style={{flex: 0.1}}></Right>
      </Header>
      <Content 
      contentContainerStyle={{
        flex: 1, 
        alignItems:'center', 
        justifyContent:'center'}}>
          <Text>Profile Screen</Text>

      </Content>
    </Container>

  );
}

const AppDrawer =() =>{
  return(
    <Drawer.Navigator>
      <Drawer.Screen 
        name="Home"
        component={HomeScreen}
        options={{
          drawerIcon:({focused, color, size})=>(
          <Icon name='people' style={{fontSize: size, color: color}}/>
        )
      }
    }/>
    <Drawer.Screen 
        name="Profile"
        component={ProfileScreen}
        options={{
          drawerIcon:({focused, color, size})=>(
          <Icon name='people' style={{fontSize: size, color: color}}/>
        ),
      }
    }
    />
    </Drawer.Navigator>
  )
}
export default class  App extends Component{
  render(){
  return(
    <NavigationContainer>
      <AppDrawer></AppDrawer>
    </NavigationContainer>
  )
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
