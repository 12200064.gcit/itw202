import React from 'react'
import {StyleSheet, View, Text, Image} from 'react-native'

export default function ImageComponent (){
  return (
    <View style={styles.container}>
        <Image source ={require('./../assets/icon.png')} style={{height: 200, width: 200}}></Image>
    </View>
  )
}


const styles =StyleSheet.create({
    container:{
        justifyContent:'center',
        alignItems:'center',
        flex: 1,
        flexDirection:'row',
    }
})
