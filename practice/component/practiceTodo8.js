import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default function Practice() {
    return (
        <View style={styles.container}>
       
            <Text>The quick <Text style={{fontWeight: 'bold'}}>brown fox jump </Text>over the lazy dog</Text>
        
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',

    }

})