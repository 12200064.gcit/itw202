import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import { useState } from 'react';

const ChangeText =()=>{
    const [text, setText] =useState();
    return (
        <View>
            <Text>Hi {text} </Text>
            <TextInput secureTextEntry placeholder='Enter the text' keyboardType='number-pad' onChangeText={(value) => setText(value)}></TextInput>
        </View>
    )
}
export default ChangeText;