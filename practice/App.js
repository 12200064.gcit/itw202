import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyButtons from './component/count';
// import ChangeText from './component/changeText';
// import Todo7practice from './component/precticeTodo7';
// import Practice from './component/practiceTodo8';
// import ImageComponent from './component/imageComponent';
export default function App() {
  return (
    <View style={styles.container}>
      {/* <Todo7practice></Todo7practice>
       <Practice></Practice> 
       <ImageComponent></ImageComponent> 
       <ChangeText></ChangeText>
       <StatusBar style="auto" /> */}
       <MyButtons buttontext='PRESS ME'></MyButtons>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
