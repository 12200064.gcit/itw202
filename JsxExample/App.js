import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import Mycomponent from './component/MyComponent';


export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.style1}>
        <Text>1</Text>
      </View>
      <View style={styles.style2}>
        <Text>2</Text>
      </View>
      <View style={styles.style3}>
        <Text>3</Text>
      </View>
    </View>
  
    
  )    
    
}

const styles = StyleSheet.create({
  container: {
    display:'flex',
    flex: 1,
    flexDirection:'row',
    paddingTop:80,
    paddingLeft:20,
    backgroundColor: '#fff',
    
    
    
  },
  style1:{
    display:'flex',
    backgroundColor:'red',
    justifyContent:'center',
    alignItems:'center',
    height:250,
    width:80,
  },
  style2:{
    display:'flex',
    backgroundColor:'blue',
    justifyContent:'center',
    alignItems:'center',
    height:250,
    width:150,
  },
  style3:{
    display:'flex',
    backgroundColor:'green',
    justifyContent:'center',
    alignItems:'center',
    height:250,
    width:20,
  }
  
});

