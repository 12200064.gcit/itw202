import React from 'react';
import {View, Text, StyleSheet} from "react-native";
 const Mycomponent =() =>{
     const greeting ='Sonam Wangmo';
     const greeting1 =<Text>Jigme Wangmo</Text>
     return (
         <View>
             <Text style= {styles.textStyle} >This is my demo of JSX</Text>
             <Text>HI there!!!!{greeting}</Text>
             {greeting1}
         </View>
     )
 };
const styles=StyleSheet.create({
     textStyle:{
         frontSize:24,
         
     }
 })
 export default Mycomponent;