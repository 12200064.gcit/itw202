import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import CountClass from './component/countClass';
import Courses from './component/courses';
export default function App() {
  return (
    <View style={styles.container}>
      {/* <Courses></Courses> */}
      <CountClass/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  CountClass:{
    textAlign:'center',
    fontSize:24,
    fontWeight:'bold',
    color:'#black',
    backgroundColor:'coral',

  }
});
