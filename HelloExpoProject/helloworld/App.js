import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {add, multiply} from './component/nameExport';
import division from './component/defaultExport';
import Courses from './component/functionalCoupo';

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Text>The result of addition: {add(5,6)}</Text>
      <Text>The result of multiplication: {multiply(5,6)}</Text>
      <Text>The result of division: {division(5,6)}</Text>
      <StatusBar style="auto" /> */}
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
