import {Dimensions} from 'react-native'
const {width, height} =Dimensions.get('window');

export const COLORS ={
    primary: '#252c4a',
    secondary:'#ece906',
    accent:'#072552',
    success:'#06ec19',
    error: '#ec0606',
    black:'#171717',
    white:'#ffffff',
    background:'#6db8e3',
    yellow:'#ece906',
    black:'#0a0a0a',
    
}

export const SIZES ={
    base:50,
    width,
    height,
}