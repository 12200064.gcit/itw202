import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

function Todo_8() {
  return (
    <View style={styles.container}>
      <Text >The<Text style={{fontWeight:'bold'}}> quick brown fox </Text> jumps over the lazy dog</Text>
      <StatusBar style="auto" />
    </View>
  );
}
export default Todo_8

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
