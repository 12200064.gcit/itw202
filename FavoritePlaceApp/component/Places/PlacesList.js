import React from 'react'
import {FlatList,StyleSheet, Text, View} from 'react-native'
import { Colors } from '../../constants/Colors'

import PlaceItems from './PlaceItems'

const PlacesList =({places})=> {
    if(!places || places.length===0){
        return (
        <View style={styles.fallbackContainer}>
            <Text style={styles.fallbackText}>No Place added yet -start adding some!</Text>
        </View>
        )
    }
  return (
    <FlatList
    data={places}
    keyExtractor ={(item) => item.id}
    renderItem={({item}) => <PlaceItems place ={item}/>}
    />
  )
}

export default PlacesList
const styles =StyleSheet.create({
    fallbackContainer:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    fallbackText:{
        fontSize:16,
        color: Colors.primary200,
    }
})