import React from 'react'
import OutlinedButton from '../UI/OutlinedButton'
import {View, Text, StyleSheet} from 'react-native'
import { Colors } from '../../constants/Colors'
import { Wrapper, Status, Map, GoogleApiWrapper, Market } from "@googlemaps/react-wrapper";

import { getCurrentPositionAsync, useForegroundPermissions, PermissionStatus } from 'expo-location'
function LocationPicker() {
    const [locationPermissionInformation, requestPermission]=useForegroundPermissions();
    async function verifyPermissions(){
        if(
            locationPermissionInformation.status === PermissionStatus.UNDETERMINED
        ){
            const permissionResponse =await requestPermission();
            return permissionResponse.granted;
        }
        if(locationPermissionInformation.status === PermissionStatus.DENIED){
            Alert.alert(
                'Insufficent Permission',
                'You need to grant location permission to use this app.'
            );
            return false;
        }
        return true;
    }
    async function getLocationHandeler(){
        const hasPermission = await verifyPermissions();
        if(!hasPermission){
            return;
        }
        const location = await getCurrentPositionAsync();

        console.log(location);
    }
    function pickOnMapHandeler(){}
  return (
    <View>
        <View style={styles.mapPreview}></View>
        <View style={styles.actions}>
            <OutlinedButton icon='location' onPress={getLocationHandeler}>
                Location User
            </OutlinedButton>
            <OutlinedButton icon='map' onPress={pickOnMapHandeler}>
                Pick on map
            </OutlinedButton>
        </View>
    </View>
  )
}

export default LocationPicker

const styles =StyleSheet.create({
    mapPreview:{
        width:'100%',
        height:200,
        marginVertical:8,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.primary100,
        borderRadius:4,
        
    },
    actions:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
})