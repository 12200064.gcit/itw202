import { Button, StyleSheet,Text,View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stacks = createStackNavigator();

function Screen1({navigation}) {
  // console.log(navigation)
  return(
    <View style={styles.container}>
      <Text>Screen1</Text>
      <Button
        title="Go to Second screen"
        onPress={()=> 
          navigation.navigate(Screen2)}
        />
        {/* <Button
          title="go to third screen"
          onPress={()=>
          {
            navigation.reset({
              index: 0,
              routes:[{name:'Screen3'}]
            })
          }}
        /> */}
    </View>
  )
}


function Screen2({navigation}){
  return(
    <View style={styles.container}>
      <Text>Screen2</Text>
      {/* <Button
        title='back'
        onPress={()=> {
          navigation.goBack();
        }}
        /> */}
         <Button
        title='go to third'
        onPress={()=> {
          navigation.replace('Screen3');
        }}
        />
     
    </View>
  )
}
function Screen3({navigation}){
  return(
    <View style={styles.container}>
      <Text>Screen3</Text>
      <Button 
      title="go back"
      onPress={()=>{
        navigation.goBack();
      }}
      />
    </View>
  )
}
 
export default function App() {
  return (
    <NavigationContainer>
      <Stacks.Navigator
      initialRouteName="Screen1"
      screenOptions={{
        headerShown:false,
      }}
      >
        <Stacks.Screen name="Screen1" component={Screen1}/>
        <Stacks.Screen name="Screen2" component={Screen2}/>
        <Stacks.Screen name="Screen3" component={Screen3}/>

      </Stacks.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
