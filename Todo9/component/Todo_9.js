import React from "react";
import {Image ,View,StyleSheet,Text} from 'react-native';

const Todo_9=()=>{
    return(
        <View style={styles.container}>
        <Image source={{uri: 'https://picsum.photos/100/100'}} style={styles.img1}/>
        <Image source={require('../assets/reactnative.png')} style ={styles.img2}></Image>
        </View>
     
        )
};

export default Todo_9;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    img1: {
        width: 100,
        height: 100,
        resizeMode: 'contain'
    },
    img2: {
        width: 100,
        height: 100,
        resizeMode: 'contain'
    },
});