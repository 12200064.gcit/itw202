import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from 'react-native';

export default function App() {
  return (
    <ScrollView>
      <Text>First Text</Text>
      <View>
        <Text>Second Text</Text>
        <Image source={{uri:"https://scontent.fpbh1-1.fna.fbcdn.net/v/t39.30808-6/277570879_522270179472572_7589943074176884294_n.jpg?_nc_cat=109&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=dZpEynYFqk4AX9T5tln&_nc_ht=scontent.fpbh1-1.fna&oh=00_AT8cK4PC0WoHdyeNKseAcgNAQuoSHqlAUYjvW2ys_CIITw&oe=62493580", }} 
        style={{width: 400, height: 400}}/>

      </View>
      <TextInput defaultValue='Type Here'/>
      <Button onPress={() =>{
        alert('You have click the button!!!');
      }}
      title="Close your eye and press me"/> 
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
