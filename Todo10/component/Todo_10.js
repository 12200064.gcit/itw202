import React  from "react";
import {useState} from 'react'
import {Text,View,TextInput, StyleSheet} from 'react-native'


const Todo_10=()=>{
    const [text,setText]=useState();
    return(
        <View style={styles.container}>
            <Text>Hi {text}! Have a good day.</Text>
            <TextInput secureTextEntry placeholder="Enter The text " onChangeText={(value)=>setText(value)}/>
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
export default Todo_10;
