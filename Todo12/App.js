import { setStatusBarBackgroundColor } from 'expo-status-bar';
import React ,{useState} from 'react';
import { Button,StyleSheet, TextInput, View ,Text,FlatList} from 'react-native';
import AspirationInput from './components/Aspirationinput';
import { AspirationItem } from './components/AspirationItem';



export default function App() {
 
  const [courseAspiration , setCourseAspiration]=useState([]);
  const [isAddMode,setIsAddMode]=useState(false);

 
  const addAspirationHandler= aspirationtitle =>{
    
    setCourseAspiration(currentAspiration =>[...currentAspiration,{key: Math.random().toString(),value:aspirationtitle}])
    setIsAddMode(false)
  }
  const removeApsirationHandler=aspirationkey=>{
    setCourseAspiration(currentAspiration=>aspirationkey!==aspirationkey)
  }
  return (
    <View style={styles.Screen}>
     <Button title=' Add New Aspiration' onPress={() => setIsAddMode(true)} />
      <AspirationInput  visible={isAddMode} onAddAspiration={addAspirationHandler}></AspirationInput>
    
    <FlatList
    data={courseAspiration}
    renderItem={itemData=>
    <AspirationItem onDelete={removeApsirationHandler}
    title={itemData.item.value}></AspirationItem>}/>
    </View>
  );
}

const styles = StyleSheet.create({
  Screen:{
    padding:50,
  
  },


  

});
