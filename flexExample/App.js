import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  const name="Madav Dhaurali";
  return (
    <View style={styles.container}>
      <Text style={styles.startnative}>Getting started with react native!</Text>
      <Text style={styles.name}>My name is {name}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  startnative:{
    fontSize:45,

  },
  name:{
    fontSize:20,
    flex:1,
  }
});
