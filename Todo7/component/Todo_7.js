import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

function Todo_7() {
  return (
    <View style={styles.container}>
      <View style={styles.style1}></View>
      <View style={styles.style2}></View>
    </View>
  );
}
export default Todo_7
const styles = StyleSheet.create({
  container: {
    display:'flex',
    flex: 1,
    flexDirection:'column',
    paddingTop:300,
    paddingLeft:125,
    backgroundColor: '#fff',
    
    
    
  },
  style1:{
    display:'flex',
    backgroundColor:'red',
    justifyContent:'center',
    alignItems:'center',
    height:80,
    width:80,
  },
  style2:{
    display:'flex',
    backgroundColor:'blue',
    justifyContent:'center',
    alignItems:'center',
    height:80,
    width:80,
  },
  style3:{
    display:'flex',
    backgroundColor:'green',
    justifyContent:'center',
    alignItems:'center',
    height:250,
    width:20,
  }
  
});

