import React from "react";
import { StyleSheet } from 'react-native';
import { Button as PapperButton } from 'react-native-paper';


export default function Button({ mode, style, ...props }) {
    return (
        <PapperButton
        style = {[
            styles.button,
            mode === 'outlined' && {backgroundColor: 'white' },
            style,
        ]}
        labelStyle = {styles.text}
        mode = {mode}
        {...props} />
    )
}

const styles = StyleSheet.create ({
    button: {
        
        width: '100%',
        marginVertical: 10,
        paddingVertical: 5,
        borderRadius: 25,

        
    },
    text: {
        fontWeight: 'bold',
        fontSize: 30,
        // lineHeight: 26,
        color: 'black',
    },
})