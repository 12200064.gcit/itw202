import React from "react";
import { TouchableOpacity, Image, StyleSheet} from "react-native";
import {getStatusBarHeight} from "react-native-status-bar-height";
export default function GoBackButton ({onPress}){
    return (
        <TouchableOpacity 
        onPress={onPress}
        style={styles.container}>
            <Image source={require("../../assets/arrow.png")}
            style={styles.image} />
        </TouchableOpacity>
    );
}
const styles =StyleSheet.create({
    container: {
        position: "absolute",
        top: 10 + getStatusBarHeight(),
        left: '5%',
        top: '6%',
        
        // alignItems: 'center'
    },
    image: {
        width: 20,
        height: 30,
    
    },
});
