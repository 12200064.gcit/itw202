import React from "react";
import { TouchableOpacity, Image, StyleSheet} from "react-native";
import {getStatusBarHeight} from "react-native-status-bar-height";
export default function HomeButton({onPress}){
    return (
        <TouchableOpacity 
        onPress={onPress}
        style={styles.container}
        >
            <Image source={{uri: 'http://cdn.onlinewebfonts.com/svg/img_159611.png'}}    
            style={styles.image} 
            
            />
        </TouchableOpacity>
    );
}
const styles =StyleSheet.create({
    container: {
        position: "absolute",
        top: 7 + getStatusBarHeight(),
        right: 4,
    },
    image: {
        width: 35,
        height: 35,
    },
});