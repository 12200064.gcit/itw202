import React from "react";
import { View, Text, StyleSheet, Image} from 'react-native';
import Background from "../components/Background";

import BackButton from "../components/BackButton";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function TwoScreen({navigation}){
    return (
<Background>

<BackButton goBack={navigation.goBack} />

<View style={styles.container1}>
<View style={styles.logo1}>
    <Logo/> 
</View> 

<View style={styles.text1}>     
    <Text style={styles.text2}>སེམས་ཅན་གི་སྲུང་།</Text>
</View> 
</View>


<View style={styles.container2}>

<View style={styles.t1}>
    <TouchableOpacity
    onPress={() => navigation.navigate("Book1")}>
        <Image style={styles.b1} source={require("../../assets/book1.png")}/>
    </TouchableOpacity>
</View>

<View style={styles.t2}>
    <TouchableOpacity
    onPress={() => navigation.navigate("Book2")}>
        <Image style={styles.b1} source={require("../../assets/book3.png")}/>
    </TouchableOpacity>
</View>

<View style={styles.t3}>
    <TouchableOpacity
    onPress={() => navigation.navigate("Book2")}>
        <Image style={styles.b1} source={require("../../assets/book2.jpg")}/>
    </TouchableOpacity>
</View> 


<View style={styles.t4}>
    <TouchableOpacity
    onPress={() => navigation.navigate("Book2")}>
        <Image style={styles.b1} source={require("../../assets/book4.jpg")}/>
    </TouchableOpacity>
</View>

</View>


</Background>

)
}

const styles=StyleSheet.create({
text2: {
fontSize: 40,
fontWeight:'bold',
paddingHorizontal: 12,
},
logo1: {
top: '8%',
},

text1: {
top: '5%',

},
b1: {
width: 130,
height: 150,

},
t1: {
top: '13%',
right: '30%',
// bottom: 50,
// flex: 1,
// alignItems: 'center',
// justifyContent: 'space-evenly',
// flexDirection: 'row',
// flexWrap: 'wrap',
// paddingTop: 20,
// top: '-5%',
},
t2: {
bottom: '12%',
left: '30%',
},
t3: {
bottom: '5%',
right: '30%',
},
t4: {
left: '29%',
bottom: '30%',
},
container1: {
top: '13%',
alignItems: 'center'
},
container2: {
top: '7%'
},


});