import React from "react";
import Background from "../components/Background";
import Button from "../components/Button";

export default function HomeScreen({navigation}){
    return (
        <Background>
        
        <Button mode="outlined"
        onPress = {() => {
            navigation.navigate("OneScreen")
        }}
        >དགའ་བིའ་སྲུང་།</Button>

        <Button mode="outlined"
        onPress = {() => {
            navigation.navigate("TwoScreen")
        }}
        >སེམས་ཅན་གི་སྲུང་།</Button>

        </Background>
        
    )
}

