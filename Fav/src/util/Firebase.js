import {firebase} from '@firebase/app'

const CONFIG = {

  apiKey: "AIzaSyBb_P1TyKIozwOyoGwNIpdlvzWuzsA-zKE",

  authDomain: "fave-d70b9.firebaseapp.com",

  databaseURL: "https://fave-d70b9-default-rtdb.firebaseio.com",

  projectId: "fave-d70b9",

  storageBucket: "fave-d70b9.appspot.com",

  messagingSenderId: "340548470057",

  appId: "1:340548470057:web:b94a9681ae342929cb6338"


};

firebase.initializeApp(CONFIG);


export default firebase;
