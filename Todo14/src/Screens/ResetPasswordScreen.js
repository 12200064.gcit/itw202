import React, {useState}from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Header from '../component/Header';
// import { Button } from 'react-native-paper';
import Paragraph from '../component/Paragraph';
import Background from '../component/Background';
import Logo from '../component/Logo';
import TextInput from '../component/Textinput';
import { emailValidator } from '../core/emailValidator';
import { passwordValidator } from '../core/passwordValidator';
import { nameValidator } from '../core/nameValidator';
import BackButton from '../component/BackButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../component/Button';

export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] =useState({value: "", error: ""})
    

    const onSubmitPressed =() => {
        const emailError =emailValidator(email.value);
        
        if(emailError) {
            setEmail({...email, error: emailError});
            
        }

    }

    return (
        <Background>
            <BackButton goBack={navigation.goBack}></BackButton>
            <Logo></Logo>
            <Header>Restore Password</Header>
            <TextInput label="Email" value={email.value} error={email.error} errorText={email.error} onChangeText={(text) => setEmail({value: text, error: ""})} description="You will receive email with password reset link"></TextInput>
            
            <Button mode="contained" onPress={onSubmitPressed}>Send Instruction</Button>   
            
        </Background>
        
    )
}
const styles =StyleSheet.create({
    
    row : {
       
        marginTop:4,
        flexDirection: "row",

    },

    link: {
        fontWeight:"bold",
        color:'blue',
        // color: theme.colors.primary,
    },
});