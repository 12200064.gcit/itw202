import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import Header from '../component/Header';


import Background from '../component/Background';

export default function ProfileScreen() {
     return (
         <Background>
             <Header>Profile</Header>
         </Background>
     )
     
}
const styles =StyleSheet.create({
    image : {
        width:110,
        height:110,
        marginBottom:8,
        resizeMode: 'contain'
        
    },
   
});