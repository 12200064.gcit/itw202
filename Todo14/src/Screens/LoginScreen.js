import React, {useState}from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Header from '../component/Header';
import Paragraph from '../component/Paragraph';
import Background from '../component/Background';
import Logo from '../component/Logo';
import TextInput from '../component/Textinput';
import { emailValidator } from '../core/emailValidator';
import { passwordValidator } from '../core/passwordValidator';
import { nameValidator } from '../core/nameValidator';
import BackButton from '../component/BackButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../component/Button';
import { theme } from '../core/theme';
import { loginUser } from '../../api/auth-api';

import { asin } from 'react-native-reanimated';

    



export default function LoginScreen({navigation}){
    const [email, setEmail] =useState({value: "", error: ""})
    const [password, setPassword] =useState({value: '', error: ''})
    const [loading,setLoading]=useState()

    const onLoginPressed =async() => {
        const emailError =emailValidator(email.value);
        const passwordError =passwordValidator(password.value)

        if(emailError || passwordError) {
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
        }
        setLoading(true)
      const response=await loginUser({
        email:email.value,
        password:password.value
      })
      if(response.error){
        alert(response.error)
      }
      else{
        navigation.replace('HomeScreen')
      }
      setLoading(false)
      // else{
      //   navigation.navigate("HomeScreen")
      // }
      
    }

    return (
        <Background>
            <BackButton goBack={navigation.goBack}></BackButton>
            <Logo></Logo>
            <Header>Welcome</Header>
            <TextInput label="Email" value={email.value} error={email.error} errorText={email.error} onChangeText={(text) => setEmail({value: text, error: ""})}></TextInput>
            <TextInput label="Password" value={password.value} error={password.error} errorText={password.error} onChangeText={(text) => setPassword({value: text, error: ""})} secureTextEntry></TextInput>

            <View style={styles.forgotPassword}>
                
                <TouchableOpacity onPress={() =>navigation.navigate("ResetPasswordScreen")}>
                    <Text style={styles.forgot}>Forgot your password</Text>
                </TouchableOpacity>
            </View>         

            <Button loading={loading} mode="contained" onPress={onLoginPressed}>Login</Button>   
            <View style={styles.row}>
                <Text>Don't have an account?</Text>
                <TouchableOpacity onPress={() => navigation.replace("RegisterScreen")}>
                    <Text style={styles.link}>Sign Up</Text>
                </TouchableOpacity>
            </View> 
        </Background>
        
    )
}
const styles =StyleSheet.create({
    
    row : {
       
        marginTop:4,
        flexDirection: "row",
        padding:10,      

    },

    link: {
        fontWeight:"bold",
        // color:'blue',
        paddingLeft:10,
        color: theme.colors.primary,
    },
    forgotPassword :{
        width: '100%',
        alignItems: 'flex-end',
        
        marginBottom: 24,
    },
    forgot:{
        fontSize: 13,
        color: 'blue',
        
    }
});