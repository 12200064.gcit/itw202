import React, {useState}from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Header from '../component/Header';
import Paragraph from '../component/Paragraph';
import Background from '../component/Background';
import Logo from '../component/Logo';
import TextInput from '../component/Textinput';
import { emailValidator } from '../core/emailValidator';
import { passwordValidator } from '../core/passwordValidator';
import { nameValidator } from '../core/nameValidator';
import BackButton from '../component/BackButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../component/Button';
import { signUpUser } from '../../api/auth-api';
 


export default function RegisterScreen({navigation}){
    const [email, setEmail] =useState({value: "", error: ""})
    const [password, setPassword] =useState({value: '', error: ''})
    const [name, setName] =useState({value: '', error: ''})

    const onSignUpPressed = async() => {
        const nameError = nameValidator(name.value);
        const emailError =emailValidator(email.value);
        const passwordError =passwordValidator(password.value)

        if(emailError || passwordError || nameError) {
            setName({...name, error: nameError});
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
        }
        const response =await signUpUser({
            name: name.value,
            email: email.value,
            password: password.value
        })
        if (response.error){
            alert(response.error)
        }else{
            console.log(response.user.displayName)
            alert(response.user.displayName)
        }

    }

    return (
        <Background>
            <BackButton goBack={navigation.goBack}></BackButton>
            <Logo></Logo>
            <Header>Create Screen</Header>
            <TextInput label="Name" value={name.value} error={name.error} errorText={name.error} onChangeText={(text) => setName({value: text, error: ""})}></TextInput>
            <TextInput label="Email" value={email.value} error={email.error} errorText={email.error} onChangeText={(text) => setEmail({value: text, error: ""})}></TextInput>
            <TextInput label="Password" value={password.value} error={password.error} errorText={password.error} onChangeText={(text) => setPassword({value: text, error: ""})} secureTextEntry></TextInput>
            <Button mode="contained" onPress={onSignUpPressed}>Sign Up</Button>  
            <View style={styles.row}>
                <Text>Already have an account?</Text>
                <TouchableOpacity onPress={() =>navigation.replace("LoginScreen")}>
                    <Text style={styles.link}>Login</Text>
                </TouchableOpacity>

            </View>      
        </Background>
        
    )
}
const styles =StyleSheet.create({
    row : {
       
        marginTop:4,
        flexDirection: "row",

    },

    link: {
        fontWeight:"bold",
        color:'blue',
        // color: theme.colors.primary,
    },
});