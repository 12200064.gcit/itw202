import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import Header from '../component/Header';

import { logoutUser } from '../../api/auth-api';


import Background from '../component/Background';

const HomeScreen = ({navigation}) => {
    return (
       <Background>
          <Header>Home</Header>
          <Button mode='contained' 
            onPress={()=>{
              logoutUser()
            }}
          >Logout</Button>
      </Background>
    )
  }
  
  export default HomeScreen
  
  const styles = StyleSheet.create({})
  