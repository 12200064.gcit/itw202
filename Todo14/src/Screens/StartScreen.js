import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Header from '../component/Header';
import { BottomNavigation } from 'react-native-paper';
import Paragraph from '../component/Paragraph';
import Background from '../component/Background';
import Logo from '../component/Logo';
import Button from '../component/Button';
export default function StartScreen({navigation}){
    return (
        <Background>
            <Logo></Logo>
            <Header>login template</Header>
            <Paragraph>
                The easiest way to start with your amazing application.
            </Paragraph>
            <Button mode="outlined" onPress ={() => {navigation.navigate("LoginScreen")}}>Login</Button>
            <Button mode="contained" onPress ={() => {navigation.navigate("RegisterScreen")}}>Sign Up</Button>
        </Background>
        
    )
}
const styles =StyleSheet.create({
    // container : {
    //     flex:1,
    //     backgroundColor:"#fff",
    //     alignItems:"center",
    //     justifyContent:"center",
    //     width:"100%",
    // }
});