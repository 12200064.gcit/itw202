import React from "react";
import { TouchableOpacity, Image, StyleSheet} from "react-native";
import {getStatusBarHeight} from "react-native-status-bar-height";

export default function BackButton ({goBack}){
    return (
        <TouchableOpacity onPress={goBack} style={styles.container}>

            <Image source={{uri: 'https://cdn-icons-png.flaticon.com/512/93/93634.png'}}
            style={styles.image} />
        </TouchableOpacity>
    );
}

const styles =StyleSheet.create({
    container: {
        position: "absolute",
        top: 10 + getStatusBarHeight(),
        left: 4,
    },
    image: {
        width: 24,
        height:24,
    },
});