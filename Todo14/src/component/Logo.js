import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import Header from './Header';
import Button from '../component/Button';
import Paragraph from './Paragraph';
import Background from './Background';

export default function Logo() {
     return <Image source={require('../../assets/logo.png')} style={styles.image}/>
     //<Image source={{uri: 'https://reactjs.org/logo-og.png'}}
    // style={{width: 200, height: 200}} />
     
}
const styles =StyleSheet.create({
    image : {
        width:110,
        height:110,
        marginBottom:8,
        resizeMode: 'contain'
        
    },
   
});