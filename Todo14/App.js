import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Provider } from 'react-native-paper';
import {theme} from './src/core/theme';
import {NavigationContainer, TabActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import TextInput from './src/component/Textinput';
import Header from './src/component/Header';
import {LoginScreen, StartScreen, RegisterScreen, ResetPasswordScreen, HomeScreen, ProfileScreen, AuthLoadingScreen, DrawerContent} from './src/Screens';
import Logo from './src/component/Logo';
import Button from './src/component/Button';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import firebase from 'firebase/app'
import {createDrawerNavigator} from '@react-navigation/drawer';


import { firebaseConfig } from './config';

if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig)
}




const Stack =createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer =createDrawerNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouterName='AuthLoadingScreen'
          screenOptions={{headerShown: false}}>     
          <Stack.Screen name='AuthLoadingScreen' component={AuthLoadingScreen}/>
          <Stack.Screen name='StartScreen' component ={StartScreen}/>
          <Stack.Screen name='LoginScreen' component={LoginScreen}/>
          <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
          <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
          <Stack.Screen name='HomeScreen' component={DrawerNavigator}/>
        </Stack.Navigator>
       </NavigationContainer>
    </Provider>
  );
}
function BottomNavigation(){
  return (
    <Tab.Navigator>
      <Tab.Screen name='Home' component={HomeScreen} options={{ tabBarIcon: ({size}) =>{return( <Image style={{width: size, height: size}} source={require('./assets/home-icon.png')}/>);}}}/>
      <Tab.Screen name='Profile' component={ProfileScreen} options={{ tabBarIcon: ({size}) =>{return( <Image style={{width: size, height: size}} source={require('./assets/setting-icon.png')}/>);}}}/>
    </Tab.Navigator>
  )
}

const DrawerNavigator = () =>{
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='BottomNavigation' component={BottomNavigation}/>
    </Drawer.Navigator>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
