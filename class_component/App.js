import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.component {
  return =() =>(
    <View style={styles.container}>
      <Text>Practical 2!</Text>
      <Text>Stateless and statefull components</Text>
      <Text style={styles.text}>You are ready to start the journey</Text>
     
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
