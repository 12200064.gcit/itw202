import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { createDrawerNavigator, DrawerContent } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import  {createStackNavigator} from '@react-navigation/stack';
import { HomeScreen, StartScreen } from '../Todo14/src/Screens';

const Tap = createBottomTabNavigator;
const Stack = createStackNavigator;
const Drawer = createDrawerNavigator;

export default function App() {
  return (
    <Provider theme ={theme}>
      <NavigationContainer>
         <Stack.Navigator
           initialRouteName ='StartScreen'
           screenOptions ={{headerShow: false}}>
             <Stack.Screen name='StartScreen' component={StartScreen}/>
             <Stack.Screen name ='HomeScreen' component ={DrawerNavigator}/> 
         </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
const DrawerNavigator = () => {
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
        <Drawer.Screen name='HomeScreen' BottomNavigation></Drawer.Screen>
    </Drawer.Navigator>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
