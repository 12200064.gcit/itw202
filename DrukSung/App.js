import { StatusBar } from 'expo-status-bar';
// import BackButton from './src/components/BackButton';
// import { Button } from 'react-native-paper';
import HomeButton from './src/components/HomeButton';
// import PrimaryButton from './src/components/PrimaryButton';
import ImageScreen from './src/screens/imageScreen';
import ForwardButton from './src/components/ForwardButton';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';


export default function App() {
  return ( 
    <SafeAreaView style={styles.container}>
     
   
      <StatusBar style="auto" />
      <View style={styles.buttons}>
      <ImageScreen></ImageScreen>
      <ForwardButton></ForwardButton>
      </View>
     
      {/* <BackButton></BackButton> */}
      <HomeButton></HomeButton>
    </SafeAreaView>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'skyblue',
    alignItems: 'center',
    justifyContent: 'center',


  },
  butt:{
    width:'80%',
        borderRadius:20,
        backgroundColor: '#0B0B45',// only for first screen       
  },
  buttons:{
    flexDirection:'column-reverse'
  }
});
