import React from "react";
import { TouchableOpacity, Image, StyleSheet} from "react-native";
import {getStatusBarHeight} from "react-native-status-bar-height";

export default function HomeButton(){
    return (
        <TouchableOpacity style={styles.container}>
            <Image source={{uri: 'http://cdn.onlinewebfonts.com/svg/img_159611.png'}}
            style={styles.image} />
        </TouchableOpacity>
    );
}

const styles =StyleSheet.create({
    container: {
        position: "absolute",
        top: 15 + getStatusBarHeight(),
        right: 4,
    },
    image: {
        width: 24,
        height:24,

    },
});