import { Pressable, StyleSheet, Text, View } from 'react-native'
import React from 'react'
function PrimaryButton({children}){
  return (

    <View style={styles.buttonOuterContainer}>
    <Pressable android_ripple={{color: 'red'}} 
    style={({pressed}) => pressed ? 
            [styles.buttonInnerContainer, styles.pressed]:
            styles.buttonInnerContainer}>
    
      <Text style={styles.buttonText}>{children}</Text>
    
    </Pressable>
    </View>
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonOuterContainer:{
        backgroundColor: '#ead215',
        overflow:'hidden',
        borderRadius:15,
        paddingVertical:5,
        paddingHorizontal: 10,
        elevation:2,
        margin:25,
        padding: 7,
    },
    buttonInnerContainer:{
        borderRadius: 8,
        paddingVertical: 8,
        paddingHorizontal: 4,
        elevation:0,
    },
    buttonText:{
        color: 'black',
        textAlign: 'center',
        fontWeight:'bold',
        fontSize:20,
    },
    pressed:{
        opacity: 1.5,
    },

})