import React from "react";
import { TouchableOpacity, Image, StyleSheet} from "react-native";
import {getStatusBarHeight} from "react-native-status-bar-height";

export default function BackButton (){
    return (
        <TouchableOpacity style={styles.container}>
            
            <Image source={{uri: 'https://cdn0.iconfinder.com/data/icons/cosmo-player/40/button_backward_1-512.png'}}
            style={styles.image} />
        </TouchableOpacity>
    );
}

const styles =StyleSheet.create({
    container: {
        position: "absolute",
        top: 15 + getStatusBarHeight(),
        left: 4,
    },
    image: {
        width: 24,
        height:24,

    },
});