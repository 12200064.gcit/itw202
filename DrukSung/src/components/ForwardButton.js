import React from "react";
import { TouchableOpacity, Image, StyleSheet} from "react-native";
import {getStatusBarHeight} from "react-native-status-bar-height";

export default function ForwardButton(){
    return (
        <TouchableOpacity style={styles.container}>
            <Image source={{uri: 'https://www.shareicon.net/data/512x512/2015/10/18/658171_play_512x512.png'}}
            style={styles.image}  resizeMode="stretch"/>
        </TouchableOpacity>
    );
}

const styles =StyleSheet.create({
    container: {
        position: "absolute",
        top: 320 + getStatusBarHeight(),
        // right:9,
        left:294,
    },
    image: {
        width: 30,
        height:30,
        

    },
});