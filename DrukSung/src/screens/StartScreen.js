import React   from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { Checkbox } from 'react-native-paper';


import PrimaryButton from '../components/PrimaryButton';


function StartScreen() {
  return (
    <View style={styles.inputContainer}>
     <View style={styles.buttonsContainer}>
       <View style={styles.buttonContainer}>
          <PrimaryButton>Read myself</PrimaryButton>
       </View>
       <checkbox></checkbox>
       <View style={styles.buttonContainer}>
       <PrimaryButton>Read for me</PrimaryButton>
      
       </View>
     </View>
</View>
  );
}
export default StartScreen

const styles = StyleSheet.create({
  inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginBottom:300,
    marginTop: 300,
    padding: 15,
    backgroundColor: '#091298',
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 2, height: 0},
    shadowRadius:6,
    shadowOpacity: 0.25,
  },

buttonsContainer: {
    flexDirection: 'column',
    padding:1,
},
buttonContainer: {
    flex: 1,
    padding: 2,
}
});

