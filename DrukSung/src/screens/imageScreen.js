import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import ForwardButton from '../components/ForwardButton';


export default function ImageScreen({navigate}) {
     return(
        <View>
        <Image source={require('../../assets/logo.png')} style={styles.image}/>
        <ForwardButton></ForwardButton>
        </View>
)}
const styles =StyleSheet.create({
    image : {
        
        width:300,
        height:400,
        marginBottom:8,
        resizeMode: 'contain',
        flex: 1,
        
    },
   
});